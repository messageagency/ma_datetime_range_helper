# README #

This module provides some functionality to the core datetime_range field type, which is currently experimental. Specifically, it:

* Unrequires the end date when entering a start date. If no end date is entered, it will populate it with the start date. It is up to the theme to render a field as a single date if the start and end are identical, if thats the idea (and it is).
* Provides 2 new formatters which very simply only display the start date or end date, respectively.

TODO: It'd be nice to have this functionality:

* Show or hide the end date and decide whether it is required or not.
* Choose which date/time components are used: Year, Month, Day, Hour, Minute, Second. Right now, they are all required.
* Handle 'All day'.

Eventually, as work is continued on the datetime_range module, this helper module may not be required. Related d.o issues:

* Plan for DateTime Range experimental module: https://www.drupal.org/node/2788253
* Allow end date to be optional: https://www.drupal.org/node/2794481
* Improve the Views integration for DateRange fields: https://www.drupal.org/node/2786577
* Support for "all day"-option: https://www.drupal.org/node/2734255
* Allow configurable date attributes to collect: https://www.drupal.org/node/2699895
* Datetime range module documentation (empty right now): https://www.drupal.org/docs/8/core/modules/datetime-range
* Datetime Extras contrib module: https://www.drupal.org/project/datetime_extras (I'm not sure whats up with this module. It looks like it was removed maybe?)